<?php

use App\Http\Controllers\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware('superadmin')->group(function(){

    //create new superadmin
    Route::get('/register','SuperadminController@register');
    Route::post('/register/handlereg','SuperadminController@handlereg');

    //create new admin
    Route::get('/adminreg','SuperadminController@adminreg');
    Route::post('/adminreg/handle','SuperadminController@adminreghandle');


    //Super admin Dashboard
    Route::get('s/dashboard','SuperadminController@Shome');

});

Route::middleware('isadmin')->group(function(){

    //admin Dashboard
    Route::get('a/dashboard','AdminController@home');

    //add new User
    Route::get('a/adduser','AdminController@adduser');
    Route::post('a/adduser/handle','AdminController@handleadduser');

    //Reset Password
    Route::get('a/users/edit/{id}/resetpassword','AdminController@changepassword');
    Route::post('a/users/update/{id}/updatepassword','AdminController@updatepassword');

    //Edit admins Users 
    Route::get('a/users/edit/{id}','AdminController@edit');
    Route::post('a/users/update/{id}','AdminController@update');

    //Delete or Deativate Users
    Route::get('a/users/delete/{id}','AdminController@delete');
    Route::get('a/users/deactive/{id}','AdminController@deactive');
    Route::get('a/users/active/{id}','AdminController@active');

    Route::get('/profile/{id}','AdminController@userprofile');

});

Route::middleware('isuser')->group(function(){
    
    //Users Dashboard
    Route::get('/u/dashboard','UserController@Uhome');
    Route::get('/u/isdeactivated','UserController@isdeactivated');
});


Route::get('/profile','AuthController@profile');
Route::post('/uploadimg/{id}','AuthController@uploadimg');

Route::get('/login','AuthController@login');
Route::post('login/handellogin','AuthController@handlogin');

Route::get('/forgetpassword','AuthController@forgetPassword');
Route::post('/forgetpassword/handle','AuthController@sendResetToken');

Route::get('/newpass','AuthController@newpass');
Route::post('/newpass/handle','AuthController@handleToken');

Route::get('/logout','AuthController@logout');

Route::get('/error','AuthController@error');


