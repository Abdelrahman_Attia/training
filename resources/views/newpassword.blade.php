@extends('layouts.default')
@section('title')
    Wisely | Reset Password
@endsection

@section('login')
<form class="form-signin" action="{{url('/newpass/handle')}}" method="POST">
    @csrf

   
    <h1 class="h3 mb-3 font-weight-normal">Please Enter Your Data</h1>
    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name ="email" required autofocus>
    
    <label for="inputToken" class="sr-only">Reset Password Token</label>
    <input type="password" id="inputToken" class="form-control" placeholder="Reset Password Token" name ="resetpasswordtoken" required>
    
    <label for="inputPassword1" class="sr-only"> new Password</label>
    <input type="password" id="inputPassword1" class="form-control mb-2" name="newpass" placeholder="new Password" required>
    
    <label for="inputPassword2" class="sr-only"> confirm Password</label>
    <input type="password" id="inputPassword2" class="form-control mb-2" name="confirmpass" placeholder="confirm new Password" required>

    <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
    <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
  
</form>
    
@endsection