@extends('layouts.admin')

@section('title')
    wisely | Profile
@endsection

@section('navbar')
    @extends('layouts.navbar')
@endsection

@section('content')

<div class="container my-5">
    @if($errors->any())
<div class="errors">
    @foreach($errors->all() as $error)
        <div class="alert alert-danger" >
            {{$error}}
        </div>
    @endforeach

</div>
@endif

@if(session()->has('message'))
<div class="errors">
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
</div>
@endif


<div class="container my-5 image-container">
    <div class="row">
        <div class="col-md-5 mb-5">
            <div class="image text-center my-3 ">
                <div class="imgsrc">
                    <img src="{{asset('asset/images/profile/'.$user->image)}}" alt="" class="img-fluid m-auto profile-img">
                    <div class="uploadicon">
                        <i class="fas fa-upload"></i>
                    </div>
                </div>
                
                <form action="{{url('/uploadimg',$user->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="file" name="image" class="" id="file">
                    <button type="submit" class="btn btn-info my-2 upload-btn" id="upload-btn">Change</button>
                </form>
            </div>
        </div>

        <div class="col-md-7 my-5">
            <label for="inputFname" class="sr-only">user Name</label>
            <input type="text" id="inputFname" class="form-control mb-2 text-center" placeholder="First Name" name ="fname" value="{{$user->firstName}} {{$user->lastName}}" disabled>

        
            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="email" id="inputEmail" class="form-control mb-2 text-center" placeholder="Email address" name ="email" value="{{$user->email}}" disabled>
           

            <label for="inputPhone" class="sr-only">Phone Number</label>
            <input type="tel" id="inputPhone" class="form-control mb-2 text-center" placeholder="Phone Number" name ="phone" value="{{$user->phone}}" disabled>
          
{{-- 
            <label for="inputPhone" class="sr-only">Role</label>
            <input type="tel" id="inputPhone" class="form-control mb-4 text-center" placeholder="Phone Number" name ="phone" value="{{$user->roles->type}}" disabled>
           --}}

        </div>
    </div>
</div>
  
@endsection