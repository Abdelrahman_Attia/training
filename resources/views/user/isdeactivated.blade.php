@extends('layouts.default')
@section('title')
    Wisely | Deactivated User
@endsection

@section('logout')
<a class="btn btn-lg btn-primary logout" href="{{url('/logout')}}">logout</a>    
@endsection

@section('login')
    <h1 class="m-auto">your Account is Deactivated , contact your admin...</h1>
@endsection