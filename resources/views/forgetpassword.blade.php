@extends('layouts.default')
@section('title')
    Wisely | Forget Password
@endsection

@section('login')
<form class="form-signin" action="{{url('/forgetpassword/handle')}}" method="POST">
    @csrf

   
    <h1 class="h3 mb-3 font-weight-normal">Please Enter Your Email</h1>
    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name ="email" required autofocus>
    
    <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
    <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
  
</form>
    
@endsection