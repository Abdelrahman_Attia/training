@extends('layouts.admin')

@section('title')
    wisely | Dashboard
@endsection

@section('navbar')
    @extends('layouts.navbar')
@endsection


@section('content')
<div class="container my-5">
    <table class="table table-bordered">
        <thead class="thead-dark">
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Phone</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr>
                <td>
                    <div class="row ml-1">
                        <div class="d-inline-block mr-2">
                            <p><a href="{{url('/profile',$user->id)}}">{{$user->firstName}} {{$user->lastName}}</a></p>
                        </div>
                        <div class="d-inline-block mr-2">
                            <a href="{{url('a/users/edit',$user->id)}}">
                                <i class="fas fa-edit"></i>
                            </a>
                        </div>
                        <div class="d-inline-block mr-2">
                            <a href="{{url('a/users/delete',$user->id)}}">
                                <i class="fas fa-trash-alt"></i>
                            </a>
                        </div>
                        @if ($user->isactive == 1)
                        <div class="d-inline-block">
                            <a href="{{url('a/users/deactive',$user->id)}}">
                                <i class="fas fa-lock-open"></i>
                            </a>
                        </div>
                        @else
                        <div class="d-inline-block">
                            <a href="{{url('a/users/active',$user->id)}}">
                                <i class="fas fa-lock"></i>
                            </a>
                        </div>
                        @endif
                    </div>
                </td>
                <td scope="col"> <p>{{$user->email}}</p></td>
                <td scope="col"><p>{{$user->phone}}</p></td>
            </tr>
            @endforeach
          
        </tbody>
      </table>
</div>
@endsection