@extends('layouts.admin')

@section('title')
    wisely | New User
@endsection

@section('navbar')
    @extends('layouts.navbar')
@endsection

@section('content')

<div class="container my-5">
    @if($errors->any())
<div class="errors">
    @foreach($errors->all() as $error)
        <div class="alert alert-danger" >
            {{$error}}
        </div>
    @endforeach

</div>
@endif


<form class="form-signin text-center" action="{{url('a/users/update',$users->id)}}" method="POST">
    @csrf

    <h1 class="h3 mb-3 font-weight-normal">Edit user</h1>

    
    <label for="inputFname" class="sr-only">First Name</label>
    <input type="text" id="inputFname" class="form-control mb-2" placeholder="First Name" name ="fname" value="{{$users->firstName}}"  autofocus>

    
    <label for="inputLname" class="sr-only">Last Name</label>
    <input type="text" id="inputLname" class="form-control mb-2" placeholder="Last Name" name ="lname" value="{{$users->lastName}}">


    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" id="inputEmail" class="form-control mb-2" placeholder="Email address" name ="email" value="{{$users->email}}" >
   
    
    <label for="inputPhone" class="sr-only">Phone Number</label>
    <input type="tel" id="inputPhone" class="form-control mb-4" placeholder="Phone Number" name ="phone" value="{{$users->phone}}">
  
    <a href="{{url('/a/users/edit/'.$users->id.'/resetpassword')}}">Change Password</a>

    <button class="btn btn-lg btn-primary btn-block mt-4" type="submit">Update</button>
    <a class="btn btn-lg btn-outline-secondary btn-block" href="{{url('/a/dashboard')}}">Cancel</a>
    <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
  
</form>
</div>

@endsection