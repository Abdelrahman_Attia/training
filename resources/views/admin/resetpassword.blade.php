@extends('layouts.admin')

@section('title')
    wisely | Reset password
@endsection

@section('navbar')
    @extends('layouts.navbar')
@endsection

@section('content')
<div class="container my-5">
    @if($errors->any())
<div class="errors">
    @foreach($errors->all() as $error)
        <div class="alert alert-danger" >
            {{$error}}
        </div>
    @endforeach

</div>
@endif

@if(session()->has('message'))
<div class="errors">
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
</div>
@endif

<form class="form-signin text-center" action="{{url('a/users/update/'.$user->id.'/updatepassword')}}" method="POST">
    @csrf

    <h1 class="h3 mb-3 font-weight-normal">Reset Password</h1>

    
    <label for="inputPassword" class="sr-only"> Old Password</label>
    <input type="password" id="inputPassword" class="form-control mb-2" name="oldpass" placeholder="old Password" autofocus>
    
    <label for="inputPassword1" class="sr-only"> new Password</label>
    <input type="password" id="inputPassword1" class="form-control mb-2" name="newpass" placeholder="new Password">
    
    <label for="inputPassword2" class="sr-only"> confirm Password</label>
    <input type="password" id="inputPassword2" class="form-control mb-2" name="confirmpass" placeholder="confirm new Password">
    

    <button class="btn btn-lg btn-primary btn-block mt-4" type="submit">Update</button>
    <a class="btn btn-lg btn-outline-secondary btn-block" href="{{url('a/users/edit',$user->id)}}">Cancel</a>
    <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
  
</form>
</div>
@endsection