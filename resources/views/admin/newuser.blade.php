@extends('layouts.admin')

@section('title')
    wisely | New User
@endsection

@section('navbar')
    @extends('layouts.navbar')
@endsection

@section('content')

@if($errors->any())
<div class="errors">
    @foreach($errors->all() as $error)
        <div class="alert alert-danger" >
            {{$error}}
        </div>
    @endforeach

</div>
@endif


<form class="form-signin text-center" action="{{url('a/adduser/handle')}}" method="POST">
    @csrf

    <h1 class="h3 mb-3 font-weight-normal">Create new user</h1>

    
    <label for="inputFname" class="sr-only">First Name</label>
    <input type="text" id="inputFname" class="form-control mb-2" placeholder="First Name" name ="fname" value="{{old('fname')}}"  autofocus>

    
    <label for="inputLname" class="sr-only">Last Name</label>
    <input type="text" id="inputLname" class="form-control mb-2" placeholder="Last Name" name ="lname" value="{{old('lname')}}">


    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" id="inputEmail" class="form-control mb-2" placeholder="Email address" name ="email" value="{{old('email')}}" >
   
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" class="form-control mb-2" name="pass" placeholder="Password" value="{{old('Password')}}">
    
    <label for="inputPhone" class="sr-only">Phone Number</label>
    <input type="tel" id="inputPhone" class="form-control mb-5" placeholder="Phone Number" name ="phone" value="{{old('phone')}}">
  
    <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
    <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
  
</form>
    
@endsection