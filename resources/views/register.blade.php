@extends('layouts.default')
@section('title')
    Wisely | Register
@endsection

@section('logout')
<a class="btn btn-lg btn-primary logout" href="{{url('/logout')}}">logout</a>   
@endsection

@section('login')
<form class="form-signin" action="{{url('/register/handlereg')}}" method="POST">
    @csrf

    <h1 class="h3 mb-3 font-weight-normal">Please Fill Data</h1>

    
    <label for="inputFname" class="sr-only">First Name</label>
    <input type="text" id="inputFname" class="form-control mb-2" placeholder="First Name" name ="fname" required autofocus>

    
    <label for="inputLname" class="sr-only">Last Name</label>
    <input type="text" id="inputLname" class="form-control mb-2" placeholder="Last Name" name ="lname" required>


    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" id="inputEmail" class="form-control mb-2" placeholder="Email address" name ="email" required>
   
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" class="form-control mb-2" name="pass" placeholder="Password" required>
    
    <label for="inputPhone" class="sr-only">Phone Number</label>
    <input type="tel" id="inputPhone" class="form-control mb-5" placeholder="Phone Number" name ="phone" required>
  
    <button class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
    <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
  
</form>
    
@endsection