<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{asset('css/all.min.css')}}">
    <link rel="canonical" href="https://getbootstrap.com/docs/4.5/examples/sign-in/">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
  
    
    {{-- <link rel="stylesheet" href="{{asset('css/login.css')}}"> --}}

    
   
    <!-- Custom styles for this template -->
    <link href="signin.css" rel="stylesheet">
  </head>
  <body>

    @yield('navbar')
    

    @yield('content')



<script src="{{asset('js/app.js')}}"></script>
</body>
</html>
