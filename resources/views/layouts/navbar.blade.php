<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="{{url('/a/dashboard')}}">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
  
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        @if (Auth::user()->role_id ==1)
        <li class="nav-item active">
          <a class="nav-link" href="{{url('s/dashboard')}}">Users<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{url('/adminreg')}}">New admin</a>
        </li> 
        @else
        @if (Auth::user()->role_id ==2)
        <li class="nav-item active">
          <a class="nav-link" href="{{url('a/dashboard')}}">Users<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="{{url('a/adduser')}}">New User</a>
        </li>            
        @else
        <li class="nav-item active">
          <a class="nav-link" href="{{url('u/dashboard')}}">Home<span class="sr-only">(current)</span></a>
        </li>
        @endif
        @endif
        
        
      </ul>
        <form class="form-inline my-2 my-lg-0" action="{{url('/logout')}}" method="GET">
            <p class="mr-sm-3 profile-link"><a href="{{url('/profile')}}" >{{Auth::user()->firstName}} {{Auth::user()->lastName}}</a></p>
            <button class="btn btn-outline-dark my-2 my-sm-0" type="submit">Logout</button>
        </form>
    </div>
  </nav>