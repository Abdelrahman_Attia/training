<!DOCTYPE html>
<html>
    <form>
        <head>
            <title>Invitation Mail</title>
        </head>
        <body>
            <h1>Welcome to Wisely</h1>
            <strong>Your Reset Password Token is:</strong>
            <p>{{ $token['resetpasswordtoken'] }}</p>
   
            <p>Thank you</p>
        </body>

    </form>
</html>