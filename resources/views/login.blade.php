@extends('layouts.default')
@section('title')
    Wisely | sign in
@endsection

@section('login')
@if($errors->any())
<div class="errors">
    @foreach($errors->all() as $error)
        <div class="alert alert-danger" >
            {{$error}}
        </div>
    @endforeach

</div>
@endif

<form class="form-signin" action="{{url('login/handellogin')}}" method="POST">
    @csrf

   
    <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name ="email" autofocus>
   
    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" class="form-control" name="pass" placeholder="Password">
    
    {{-- <div class="checkbox mb-3">
      <label>
        <input type="checkbox" value="remember-me"> Remember me
      </label>
    </div> --}}
  
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
    <a href="/forgetpassword">Forget Password</a>
    <p class="mt-5 mb-3 text-muted">&copy; 2020</p>
  
</form>
@endsection