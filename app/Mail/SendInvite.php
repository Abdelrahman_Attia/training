<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendInvite extends Mailable
{
    use Queueable, SerializesModels;
    public $userInvitation;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userInvitation)
    {
        //
        $this->userInvitation = $userInvitation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->view('view.name');
        return $this->from('abdelrahman@wiselyinsure.com')
                ->subject('Invitation Mail')
                ->view('emails.invitation');
    }
}
