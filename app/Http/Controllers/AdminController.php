<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\User;
use App\Mail\SendInvite;
use Illuminate\Support\Facades\Mail;


class AdminController extends Controller
{
    //
    function home()
    {
        $cuser=Auth::user()->id;
        $users=User::where('user_parent',$cuser)->get();
        return view('admin.dashboard',compact('users'));
    }

    function adduser()
    {
        return view('admin.newuser');
    }

    function handleadduser(Request $req)
    {
        $validator=\Validator::make($req->all(),
                                   [
                'fname'=>'required|max:100|min:4',
                'lname'=>'required|max:100|min:4',
                'email'=>'email|unique:users,email|max:100|min:5',
                'pass'=>'required|max:20|min:5',
                'phone'=>'required|numeric|digits_between:8,15'                     
                                   ]);

        
        if ($validator->fails()) {
            return redirect('/a/adduser')
                        ->withErrors($validator)
                        ->withInput();
        }

        $fname=$req->fname;
        $lname=$req->lname;
        $email=$req->email;
        $password=$req->pass;
        $phone=$req->phone;

        $userInvitation = [
            'email' => $email,
            'password' => $password
        ];

        $user =new User();
        $user->firstName= $fname;
        $user->lastName= $lname;
        $user->email= $email;
        $user->password =\Hash::make($password);
        $user->phone = $phone;
        $user->user_parent=Auth::user()->id;
        $user->role_id=3;
        $user->save();

        Mail::to($email)->send(new SendInvite($userInvitation));
        
        return redirect('/a/dashboard');
    }


    function edit($id)
    {
        $users =User::find($id);
        // return $id;
        return view('admin.edit',compact('users'));
    }

    function update($id,Request $req)
    {
        $validator=\Validator::make($req->all(),
                 [
                    'fname'=>'required|max:100|min:4',
                    'lname'=>'required|max:100|min:4',
                    'email'=>'email|unique:users,email,'.$id.'|max:100|min:5',
                    // 'pass'=>'required',
                    'phone'=>'required|numeric|digits_between:8,15'                     
                ]);


        if ($validator->fails()) {
            return redirect('a/users/edit/'.$id)
                        ->withErrors($validator)
                        ->withInput();
        }

        $fname=$req->fname;
        $lname=$req->lname;
        $email=$req->email;
        // $password=$req->pass;
        $phone=$req->phone;

        $user =User::find($id);
        $user->firstName= $fname;
        $user->lastName= $lname;
        $user->email= $email;
        // $user->password =\Hash::make($password);
        $user->phone = $phone;
        $user->save();
        return redirect('/a/dashboard');
    }


    function deactive($id)
    {
        $user=User::find($id);
        $user->isactive=0;
        $user->save();
        return redirect('/a/dashboard');
    }

    function active($id)
    {
        $user=User::find($id);
        $user->isactive=1;
        $user->save();
        return redirect('/a/dashboard');
    }

    function delete($id)
    {
        $user=User::find($id);
        $user->delete();
        return redirect('/a/dashboard');
    }


    function changepassword($id)
    {
        $user =User::find($id);
        return view('admin.resetpassword',compact('user'));
    }

    function updatepassword($id,Request $req)
    {
        $validator=\Validator::make($req->all(),
                 [
                    'oldpass'=>'required|max:20|min:5',
                    'newpass'=>'required|max:20|min:5',
                    'confirmpass'=>'required|max:20|min:5|same:newpass'                    
                ]);


        if ($validator->fails()) {
            return redirect('/a/users/edit/'.$id.'/resetpassword')
                        ->withErrors($validator)
                        ->withInput();
        }


        $oldpass=$req->oldpass;
        $newpass=$req->newpass;
        $confirmpass=$req->confirmpass;
        
        $user =User::find($id);
        if (Hash::check($oldpass, $user->password)) {
            // The passwords match...
            $user->password =\Hash::make($newpass);
            return redirect()->back()->with('message', 'Change Password Success');
        }
        else{
            return redirect('/a/users/edit/'.$id.'/resetpassword')->withErrors("old password not correct");
        }
        
    }


    function userprofile($id)
    {
        $user= User::find($id);
        return view('user.userprofile',compact('user'));

    }
}
