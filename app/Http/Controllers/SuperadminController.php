<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Mail\SendInvite;
use Illuminate\Support\Facades\Mail;

class SuperadminController extends Controller
{
    //



    function register()
    {
        return view('register');
    }

    function handlereg(Request $req)
    {
        $fname=$req->fname;
        $lname=$req->lname;
        $email=$req->email;
        $password=$req->pass;
        $phone=$req->phone;

        $user =new User();
        $user->firstName= $fname;
        $user->lastName= $lname;
        $user->email= $email;
        $user->password =\Hash::make($password);
        $user->phone = $phone;
        $user->role_id=1;
        $user->save();
        return redirect ('/login');

    }


    function adminreg()
    {
        return view('admin.register');
    }


    function adminreghandle(Request $req)
    {
        $validator=\Validator::make($req->all(),
                                   [
                'fname'=>'required|max:100|min:4',
                'lname'=>'required|max:100|min:4',
                'email'=>'email|unique:users,email|max:100|min:5',
                'pass'=>'required|max:20|min:5',
                'phone'=>'required|numeric|digits_between:8,15'                     
                                   ]);

        
        if ($validator->fails()) {
            return redirect('/adminreg')
                        ->withErrors($validator)
                        ->withInput();
        }

        $fname=$req->fname;
        $lname=$req->lname;
        $email=$req->email;
        $password=$req->pass;
        $phone=$req->phone;

        $userInvitation = [
            'email' => $email,
            'password' => $password
        ];

        $user =new User();
        $user->firstName= $fname;
        $user->lastName= $lname;
        $user->email= $email;
        $user->password =\Hash::make($password);
        $user->phone = $phone;
        $user->role_id=2;
        $user->save();

        Mail::to($email)->send(new SendInvite($userInvitation));
        
        return redirect('/adminreg');

    }

    function Shome()
    {
        $users =User::get();
        return view('superadmin.dashboard',compact('users'));
    }

}
