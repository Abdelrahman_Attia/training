<?php

namespace App\Http\Controllers;

use App\Token;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\Str;
use App\Mail\SendToken;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{
    //

    function login()
    {
        return view('login');
    }

    function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

    function handlogin(Request $req)
    {
        $validator=\Validator::make($req->all(),
                 [
                    'email'=>'required|email|max:100|min:5',
                    'pass'=>'required|max:20|min:5'
                ]);


        if ($validator->fails()) {
            return redirect('/login')
                        ->withErrors('Email or Password are Wrong...');
                        
        }

        $email=$req->email;
        $pass=$req->pass;
        $cred=array('email'=>$email,'password'=>$pass);

        if(Auth::attempt($cred))
        {
            if(Auth::user()->role_id==1)
            return redirect('/s/dashboard');
            elseif(Auth::user()->role_id==2)
            return redirect('/a/dashboard');
            elseif(Auth::user()->role_id==3 && Auth::user()->isactive==1)
            return redirect('/u/dashboard');
            elseif(Auth::user()->role_id==3 && Auth::user()->isactive==0)
            return redirect('/u/isdeactivated');
        }
        else return redirect('/login')->withErrors('Email or Password are Wrong...');;
    }

    function error()
    {
        return view('error');
    }

    function forgetPassword()
    {
        return view('forgetpassword');
    }

    function sendResetToken(Request $req)
    {
        $validator=\Validator::make($req->all(), ['email'=>'email|max:100|min:5']);
        if ($validator->fails()) {
            return redirect('forgetpassword')
                        ->withErrors($validator)
                        ->withInput();
        }
        $usertoken = Token::where('email',$req->email)->count();
        if ($usertoken > 0)
        {
            return redirect('/forgetpassword')
                        ->withErrors($validator)
                        ->withInput();
        }

        $email = $req->email;
        $resettoken = Str::random(32);

        $tokenMail = [
            'email' => $email,
            'resetpasswordtoken' => $resettoken
        ];

        $token = new Token();
        $token->email = $email;
        $token->resetpasswordtoken = $resettoken;
        $token->save();

        Mail::to($email)->send(new SendToken($tokenMail));

        return redirect('newpass');
    }
    function newpass()
    {
        return view('newpassword');
    }

    function handleToken(Request $req)
    {
        $validator=\Validator::make($req->all(),
                 [
                    'email'=>'email|max:100|min:5',
                    'resetpasswordtoken'=>'required|min:32|max:32',
                    'newpass'=>'required|max:20|min:5',
                    'confirmpass'=>'required|max:20|min:5|same:newpass'
                ]);


        if ($validator->fails()) {
            return redirect('/newpass')
                        ->withErrors($validator)
                        ->withInput();
        }
        $tokenData = Token::where('email',$req->email)->where('resetpasswordtoken',$req->resetpasswordtoken)->first();
        if (!$tokenData)
            return view('/newpassword');
        $user = User::where('email', $tokenData->email)->first();
        $user->password = \Hash::make($req->newpass);
        $user->save();
        $tokenData->delete();
        return redirect('/login');
    }
    function profile()
    {
        $user=Auth::user();
        return view('profile',compact('user'));
    }

    function uploadimg($id,Request $req)
    {
        $validator=\Validator::make($req->all(),
                 [
                    'image'=>'required|image|max:10240',
                ]);


        if ($validator->fails()) {
            return redirect('/profile')
                        ->withErrors($validator)
                        ->withInput();
        }

        $image=$req->file('image');
        $imageName = time().$id.$image->getClientOriginalName();
        $img = \Image::make($image->getRealPath());
        $img->resize(150, 150);
        $img->save(public_path('asset/images/profile/'. $imageName));

        $user= User::find($id);
        $user->image=$imageName;
        $user->save();
        return redirect()->back()->with('message', 'image upload Success');
    }
}
